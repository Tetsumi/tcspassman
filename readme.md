tcspassman
===================

A stateless password generator written in C.
Generate a password from a master key and a text salt.

![Screenshot](https://abload.de/img/swpsix.png)

Requirements:
-------------
- OpenSSL
- IUP

How to compile:
---------------

`cc -lcrypto -liup main.c`

![GPLv3](https://www.gnu.org/graphics/gplv3-127x51.png) 

[Note: I am no longer working on this.]