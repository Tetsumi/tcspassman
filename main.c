/*
  tcspassman

  Contributors:
     Tetsumi <tetsumi@protonmail.com>

  Copyright (C) 2018 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <iup.h>
#include <openssl/evp.h>

EVP_MD_CTX *g_mdctx;
uint64_t g_seed[16];
int g_rindex;
Ihandle *g_masterkeyField;
Ihandle *g_saltField;
Ihandle *g_passField;
char g_password[17];

void seed(char *s, uint32_t n)
{
	EVP_DigestInit_ex(g_mdctx, EVP_sha512(), NULL);
	EVP_DigestUpdate(g_mdctx, s, strlen(s));
	EVP_DigestFinal_ex(g_mdctx, (char*)&g_seed[n], NULL);
}

uint64_t randomize(void)
{
	const uint64_t s0 = g_seed[g_rindex++];
	uint64_t s1 = g_seed[g_rindex &= 15];
	s1 ^= s1 << 31;
	s1 ^= s1 >> 11;
	s1 ^= s0 ^ (s0 >> 30);
	g_seed[g_rindex] = s1;
	return s1 * (uint64_t)1181783497276652981;
}

uint64_t randomizeRange (uint64_t min, uint64_t max)
{
	return min + randomize() / (UINT64_MAX / (max - min + 1) + 1);
}

void shuffleString(char *s)
{
	size_t len = strlen(s);
	for (uint32_t i = 0; i < len - 1; ++i)
	{
		uint32_t j = randomizeRange(i, len-1);
		char t = s[i];
		s[i] = s[j];
		s[j] = t;
	}
}

int genPassword(Ihandle *h)
{
	char *charsets[] = {(char[]){"0123456789"},
			    (char[]){"!#$%&'()*+,-.:;<=>?@[]^_`{|}~"},
			    (char[]){"ABCDEFGHIJKLMNOPQRSTUVWXYZ"},
			    (char[]){"abcdefghijklmnopqrstuvwxyz"}};
	
	seed(IupGetAttribute(g_masterkeyField, "VALUE"), 0);
	seed(IupGetAttribute(g_saltField, "VALUE"), 8);
	g_rindex = 0;    	
	for (uint32_t i = 0; i < 4; ++i)
	{
		shuffleString(charsets[i]);
		memcpy(g_password + (i * 4), charsets[i], 4);
	}	
	shuffleString(g_password);
	IupSetAttribute(g_passField, "VALUE", g_password);
	return IUP_DEFAULT;
}

void setupGui(void)
{
	// Masterkey
	Ihandle *masterkeyLabel = IupLabel("Masterkey:");
	IupSetAttribute(masterkeyLabel, "SIZE", "44x");
        g_masterkeyField = IupText(NULL);
	IupSetAttributes(g_masterkeyField,
			 "VISIBLECOLUMNS=20,"
			 "NAME=masterkey,"
			 "PASSWORD=YES");
	IupSetCallback(g_masterkeyField,
		       "VALUECHANGED_CB",
		       genPassword);
	
	// Salt
	Ihandle *saltLabel = IupLabel("Salt:");
	IupSetAttribute(saltLabel, "SIZE", "44x");
        g_saltField = IupText(NULL);
	IupSetAttributes(g_saltField,
			 "VISIBLECOLUMNS=20,"
			 "NAME=salt");
	IupSetCallback(g_saltField,
		       "VALUECHANGED_CB",
		       genPassword);
	
	// Password
	Ihandle *passLabel = IupLabel("Password:");
	IupSetAttribute(passLabel, "SIZE", "44x");
        g_passField = IupText(NULL);
	IupSetAttributes(g_passField,
			 "VISIBLECOLUMNS=20,"
			 "NAME=pass,"
			 "READONLY=YES");
	
	// Dialog
	Ihandle *dialog = IupDialog(
		IupVbox(IupHbox(masterkeyLabel, g_masterkeyField, NULL),
			IupHbox(saltLabel, g_saltField, NULL),
			IupHbox(passLabel, g_passField, NULL),
			NULL));
	IupSetAttribute(dialog, "TITLE", "Password Manager");
	IupShowXY(dialog, IUP_CENTER, IUP_CENTER);
		    
}

void initializeOpenSSL(void)
{
	g_mdctx = EVP_MD_CTX_new();
}

void finalizeOpenSSL(void)
{
	if (!g_mdctx)
		return;
	EVP_MD_CTX_free(g_mdctx);
	g_mdctx = NULL;
}

int main(int argc, char* argv[])
{
	IupOpen(&argc, &argv);
	IupSetGlobal("SINGLEINSTANCE", "tcspassman");
	if (!IupGetGlobal("SINGLEINSTANCE"))
	{
		IupClose();  
		return EXIT_SUCCESS;
	}
	initializeOpenSSL();
	setupGui();
	IupMainLoop();
	IupClose();
	finalizeOpenSSL();
	return EXIT_SUCCESS;
}
